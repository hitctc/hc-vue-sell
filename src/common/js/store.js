// 通过方案：存储localstorage
export function saveToLocal(id, key, value) {
  let seller = window.localStorage.__seller__; // 读取
  if (!seller) {
    seller = {};
    seller[id] = {};
  } else {
    seller = JSON.parse(seller);
    if (!seller[id]) {
      seller[id] = {};
    }
  }
  seller[id][key] = value;
  window.localStorage.__seller__ = JSON.stringify(seller);
}

export function loadFromLocal(id, key, def) {
  let seller = window.localStorage.__seller__; // 读取
  if (!seller) {
    // 没有seller 就返回默认值
    return def;
  }
  seller = JSON.parse(seller)[id]; // 转化这个json
  if (!seller) {
    return def;
  }
  let ret = seller[key];
  return ret || def;
}
