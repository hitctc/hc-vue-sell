import Vue from 'vue';
import App from './App';
import router from './router';
// import VueResource from 'vue-resource';


// 设置为 false 以阻止 vue 在启动时生成生产提示。
Vue.config.productionTip = false;

// Vue.use(VueResource);
// 规则的校验，如果不校验那么new Vue实例就会在终端报错
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  // components: { App }  vue1.0的写法
  // render: h => h(App)    // vue2.0的写法
  render: h => h(App)
});
