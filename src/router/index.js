import Vue from 'vue';
import Router from 'vue-router'; // import路由
import axios from 'axios';
import VueAxios from 'vue-axios';
import goods from '@/components/goods/goods';
import ratings from '@/components/ratings/ratings';
import seller from '@/components/seller/seller';

import '@/common/stylus/index.styl'; // 加载这个公共样式
Vue.use(Router); // 使用路由
Vue.use(VueAxios, axios); // 使用阿克笑死

export default new Router({
  linkExactActiveClass: 'active',
  mode: 'history',
  // base: __dirname,
  // 定义路由，每个路由映射一个组件，其中"component" 可以是通过 Vue.extend() 创建的组件构造器，或者只是一个组件配置对象
  routes: [
    // 默认重定向一个路由
    {
      path: '/',
      redirect: '/goods'
    },
    {
      path: '/goods',
      name: 'goods',
      component: goods
    },
    {
      path: '/ratings',
      name: 'ratings',
      component: ratings
    },
    {
      path: '/seller',
      name: 'seller',
      component: seller
    }
  ]
});
// Router.go('/goods');
