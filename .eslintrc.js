// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint'
  },
  env: {
    browser: true,
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/essential',
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    'standard'
  ],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  // add your custom rules here
  rules: {
    // allow async-await
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'semi': ['error', 'always'],
    'no-tabs': 0,
    'indent': 0, // 基于空格
    'space-before-function-paren': 0, // 不去检测function() {}
    'no-multiple-empty-lines': ["error", { "max": 2, "maxEOF": 2 }], // 允许中间空行最大2行，结束行最大1行
    'no-trailing-spaces': ["error", { "skipBlankLines": true }], // 跳过空格线(ps好像没什么用啊，不知道是不是没有配正确)
  }
}
